import Router from './router/app.router'
import React, {useReducer} from 'react';

import Context from "./utils/context";
import * as ACTIONS from "./actionCreators/users.action";
import {ReducerFunction, defaultState} from "./reducers/user.reducer";

export default function App() {
  const [stateUser, dispatchUserReducer] = useReducer(ReducerFunction, defaultState);

  const handleShowUser = (data) => {
    dispatchUserReducer(ACTIONS.showUser(data));
  };

  const handleCheckUser = (data) => {
    dispatchUserReducer(ACTIONS.checkUser(data));
  };

  return (
    <Context.Provider
            value={{
                userState: stateUser,
                handleShowUser: (data) => handleShowUser(data),
                handleCheckUser: (data) => handleCheckUser(data),
            }}
        >
          <Router />
        </Context.Provider>
  );
}