import { StyleSheet, Text, View, TextInput, Button } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

export default function Home({navigation}) {

    return (
      <View style={styles.container}>
        <Button style={styles.button}
            title="User info"
            color='#1E6738'
            onPress={() => navigation.navigate('Registration', {isInfo: true})}
            />
          <View style={styles.buttonUp}>
          <Button style={styles.buttonUp} 
            title="Log out"
            color='#1E6738'
            onPress={() => navigation.navigate('Login')}
            /></View>
          <StatusBar style="auto" />
    </View>
    )
}


const styles = StyleSheet.create({
    buttonUp: {
      margin: 35,
      color: '#1E6738'
    },
    button: {
        margin: 35,
        borderRadius: 10,
        padding: 10,
        elevation: 2
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: 15,
      textAlign: "center"
    },
    label: {
      margin: 8,
    },
    input: {
      height: 40,
      margin: 12,
      borderWidth: 1,
      padding: 10,
      borderColor: 'rgb(153, 108, 48)'
    },
    container: {
      flex: 5,
      fontSize: 36,
      backgroundColor: 'rgb(0, 81, 105)',
      alignItems: 'center',
      justifyContent: 'center',
    },
    text: {
      fontSize: 26,
    }
  });
  