import { StyleSheet, Text, View, TextInput, Button, Alert } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import React, { useState, useContext, useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Context from '../utils/context'

export default function Login({navigation, route}) {
    const [login, checkLogin] = useState("");
    const [password, checkPassword] = useState("");

    const context = useContext(Context);

    useEffect(() => {
      if(route.params){
        checkLogin(context.userState.login),
        checkPassword(context.userState.password)
      }
  }, [])

  const userChecker = () => {
    if(context.userState.login !== login){
      return Alert.alert('Wrong login')
    }
    if(context.userState.password !== password){
      return Alert.alert('Wrong password')
    }
  }


  const trueCheckout = () => {
    if(context.userState.login !== login && context.userState.password !== password){
      navigation.navigate('Login')
    } 
    if(context.userState.login === login && context.userState.password === password){
      navigation.navigate('Home')
    }
  }


    return (
      <View style={styles.container}>
        <Text style={styles.text}>Please, sign in:</Text>
          <View style={styles.loginBlock}>
            <Text>Login: </Text>
            <TextInput
              style={styles.input}
              onChangeText={checkLogin}
              value={login}
              placeholder="Login here"
            />
          </View>
          <View style={styles.loginBlock}>
            <Text>Password: </Text>
            <TextInput
              style={styles.input}
              onChangeText={checkPassword}
              value={password}
              placeholder="Password"
            />
          </View>

          <Button style={styles.button}
            title="Sign in"
            color='#1E6738'
            onPress={() => {context.handleCheckUser(userChecker()), trueCheckout()}}
            />
          <View style={styles.buttonUp}>
          <Button style={styles.buttonUp} 
            title="Sign up"
            color='#1E6738'
            onPress={() => navigation.navigate('Registration')}
            /></View>
          <StatusBar style="auto" />
    </View>
    )
}


const styles = StyleSheet.create({
    buttonUp: {
      margin: 35,
      color: '#1E6738'
    },
    button: {
      borderRadius: 10,
      padding: 10,
      elevation: 2
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: 15,
      textAlign: "center"
    },
    label: {
      margin: 8,
    },
    input: {
      height: 40,
      margin: 12,
      borderWidth: 1,
      padding: 10,
      borderColor: 'rgb(153, 108, 48)'
    },
    container: {
      flex: 5,
      fontSize: 36,
      backgroundColor: 'rgb(0, 81, 105)',
      alignItems: 'center',
      justifyContent: 'center',
    },
    text: {
      fontSize: 26,
    }
  });
  