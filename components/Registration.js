import { StyleSheet, Text, View, TextInput, Button } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import React, { useState, useContext, useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Context from '../utils/context'

export default function Registration({navigation, route}) {
    const [login, setLogin] = useState("");
    const [password, setPassword] = useState("");
    const [name, setName] = useState("");

    const context = useContext(Context);
    console.log(context);

    useEffect(() => {
        if(route.params.isInfo){
            setLogin(context.userState.login),
            setPassword(context.userState.password),
            setName(context.userState.name)
        }
    }, [])


    return (
      <View style={styles.container}>
        <Text style={styles.text}>Please, sign in:</Text>
          <View style={styles.loginBlock}>
            <Text>Login: </Text>
            <TextInput
              style={styles.input}
              onChangeText={setLogin}
              value={login}
              placeholder="Login here"
            />
          </View>
          <View style={styles.loginBlock}>
            <Text>Password: </Text>
            <TextInput
              style={styles.input}
              onChangeText={setPassword}
              value={password}
              placeholder="Password"
            />
          </View>
          <View style={styles.loginBlock}>
            <Text>Name: </Text>
            <TextInput
              style={styles.input}
              onChangeText={setName}
              value={name}
              placeholder="Your name"
            />
          </View>
          <View style={styles.buttonUp}>
          <Button style={styles.buttonUp}
            title={(route.params.isInfo? 'Save' : 'Sign up')}
            color='#1E6738'
            onPress={() => {context.handleShowUser({name, login, password}), navigation.navigate('Home')}}
            /></View>
          <StatusBar style="auto" />
    </View>
    )
}


const styles = StyleSheet.create({
    button: {
      borderRadius: 10,
      padding: 10,
      elevation: 2
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: 15,
      textAlign: "center"
    },
    label: {
      margin: 8,
    },
    input: {
      height: 40,
      margin: 12,
      borderWidth: 1,
      padding: 10,
      borderColor: 'rgb(153, 108, 48)'
    },
    container: {
      flex: 5,
      fontSize: 36,
      backgroundColor: 'rgb(0, 81, 105)',
      alignItems: 'center',
      justifyContent: 'center',
    },
    text: {
      fontSize: 26,
    }
  });
  