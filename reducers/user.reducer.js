import {SHOW_USER, CHECK_USER} from "../constants/user.constants";

export const defaultState = {};

export const ReducerFunction = (state = defaultState, action) => {
    switch (action.type) {
        case SHOW_USER:
            let newState = {
                name: action.data.name,
                login: action.data.login,
                password: action.data.password
            };
            return newState;
        case CHECK_USER:
            return state;
        default:
            return state;
    }
};
